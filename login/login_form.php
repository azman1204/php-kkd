<?php
session_start();
// isset() - semak samada sesuatu variable wujud / tidak
if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    echo "$username $password";

    // validation
    $pass = true;
    $msg = '';

    if (empty($username)) {
        $pass = false;
        $msg = 'User is required <br>';
    }

    if (strlen($username) < 3) {
        $pass = false;
        $msg = 'Username should more than 3 characters <br>';
    }

    if (empty($password)) {
        $pass = false;
        $msg = $msg . 'Password is required';
    }

    if ($pass) {
        include '../chap8/connection.php';
        // $sql = "SELECT * FROM person WHERE username = '$username' 
        //         AND password = '$password'";
        $sql = "SELECT * FROM person WHERE username = '$username'";
        $result = mysqli_query($mysqli, $sql);
        $count = mysqli_num_rows($result); // kira bilangan rekod dari query yg di execute
        
        if ($count > 0) {
            // redirect ke home page
            // echo 'user exist';
            $row = mysqli_fetch_object($result);
            if (password_verify($password, $row->password)) {
                $_SESSION['logged_in'] = true;
                $_SESSION['username'] = $username;
                //$person = mysqli_fetch_object($result); // lebih mcm mysqli_fetch_assoc()
                $_SESSION['name'] = $row->name;
                $_SESSION['role'] = $row->role;
                $_SESSION['person_id'] = $row->id;
                header('location:../master/home.php');
            } else {
                $msg = 'Wrong combination of Username and Password';
            }
        } else {
            $msg = 'Wrong combination of Username and Password';
            // echo 'user does not exist';
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body class="container">
    <div class="d-flex justify-content-center mt-5">
        <form method="post" action="" class="col-6 ">

            <?php if (isset($msg)) : ?>
                <div class="alert alert-danger">
                    <?= $msg ?>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-12">
                    <label>User ID</label>
                    <input type="text" name="username" class="form-control" required>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-12">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <input type="submit" value="Log Masuk" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
</body>
</html>