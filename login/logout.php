<?php
// delete all session variables
// unset($_SESSION['logged_in']);
// unset($_SESSION['username']);
// unset($_SESSION['name']);
session_start();
session_destroy();

// redirect ke login page
header('location:login_form.php');