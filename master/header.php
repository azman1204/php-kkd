<?php
session_start();
include '../login/pak_guard.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Training</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body class="container mt-2">
    <div class="bg-info">Header</div>
    <div>
        <a href="../master/home.php">Home</a> | 
        <a href="../chap8/person_list.php">Person List</a> | 
        <a href="../chap8/person_form.php">Regiter Person</a> | 
        <a href="../login/logout.php">
            Logout(<?= $_SESSION['name'] ?>)
        </a>
    </div>
    <div class="p-3">