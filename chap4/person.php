<?php
class Person {
    // by default variable dibawah value nya null
    public $name;
    public $age;
    public $salary;

    // ini constructor. guna utk initialize property
    function __construct($name, $age, $salary) {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }

    // public method by default
    function is_high_salary() {
        // $this-> utk panggil property atau 
        // method dlm class ini
        if ($this->salary > 3000) 
            return true;
        else
            return false;
    }

    private function increase_salary() {
        // increase 10% salary
        // $this->salary = $this->salary + ($this->salary * 0.1);
        $this->salary = $this->salary * 1.1;
    }

    public function newSalary() {
        if ($this->salary < 3000) {
            $this->increase_salary();
        }
    }
}
