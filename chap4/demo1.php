<?php
/**
 * biasa nama class capitalize
 */
class Dog {
    // dlm class hanya ada 2 item
    // 1. property
    // 2. method (function)

    // property
    // public / private / protected - access modifier
    public $name = 'Jhonny';

    // method
    public function bark() {
        echo "Bark..bark..";
    }
}

// instantiate
$johnny = new Dog; // new Dog()
echo $johnny->name;
echo $johnny->bark();

class Cat {
    public $name = 'oyange';
}