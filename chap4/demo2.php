<?php
// require, require_once, include, include_once
// macam copy then paster
require 'person.php';

// $p = new Person();
// $p->name = 'John Doe';
// $p->age = 40;
// $p->salary = 2000;
$p = new Person('John Doe', 40, 2000);
// $p = new Person('John Doe', $age=40, $salary=2000);
echo "Name = $p->name age = $p->age";

if ($p->is_high_salary()) {
    echo "Gaji anda dah tinggi...";
}

// error..sebab method ini private
//$p->increase_salary();
$p->newSalary();
echo '<br>' . $p->salary;