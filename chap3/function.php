<?php
$val1 = tambah(10, 20);
print($val1);

$val1 = tambah(11, 21);
print($val1);

/**
 * kebiasaanya function akan return value
 * tujuan function : code reuse
 * parameter disini wajib
 * dari segi susunan, function boleh letak di atas / bawah
 **/
function tambah($no1, $no2) {
    $no3 = $no1 + $no2;
    return $no3;
}

/**
 * sample function with optional parameters
 * $no2 adalah optional
 * optional param mesti selepas wajib param
 */
function tolak($no1, $no2 = 0) {
    return $no1 - $no2;
}

echo tolak(10, 2); // 10 - 2
echo tolak (5); // 5 - 0