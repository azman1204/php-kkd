<?php
$nama = "Azman";
echo strtoupper($nama);

$nama2 = strtoupper($nama);
$str = "nama saya " . strtoupper($nama) . "<br>";
// $str = "nama saya $nama2";
echo $str;

// http://localhost/php-kkd/chap3/builtin_function.php
// ctl + b = toggle explorer

$no1 = 10;
$no2 = 2;
$no3 = $no1 + $no2; // arithmetic
echo "$no1 + $no2 = $no3 <br>";
$no4 = $no1 * $no2;
echo "$no1 x $no2 = $no4 <br>";
$no5 = $no1 % $no2; // modulus - nilai baki
echo "$no1 % $no2 = $no5 <br>";

$no6 = 25;
$no7 = sqrt($no6);
echo "sqrt($no6) = $no7";