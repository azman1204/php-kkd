<?php
// http://localhost/php-kkd/chap3/conditional.php
$a = 25; $b = 26; // dlm single line ada 2 declaration var, ok
if ($a == $b) {
    echo "a dan b sama";
} else {
    echo "a dan b tidak sama";
}

// jika banding no, bisa kita guna ==
// jika string biasa guna ===
echo '<hr>';
$c = 20; // number
$d = "20"; // string
if ($c === $d) {
    echo "c dan d sama";
} else {
    echo "c dan d tidak sama";
}

echo PHP_EOL;

if ($c == $d) {
    echo "..c dan d sama";
} else {
    echo "..c dan d tidak sama";
}

echo PHP_EOL;

// kesimpulan:
// 1. jika compare no, always == 
// 2. jika compare string, always ===
echo "<hr>";

if ($a > $b) {
    echo "a lebih besar dari b";
} else if ($a > $c) {
    echo "a lebih besar dari c";
} else {
    echo "a lebih kecil dari c";
}

echo '<hr>';
// Logical OR
// if ($a > $b or $a > $c) {
//     echo "Yes, a > b OR a > c";
// }

if ($a > $b || $a > $c) {
    echo "Yes, a > b OR a > c";
}

// Logical AND
if ($a > $b and $a > $c) {
    echo "Yes, a > b AND a > c";
}

if ($a > $b && $a > $c) {
    echo "Yes, a > b AND a > c";
}

// not
if ($a !== $b) {
    echo "a tidak sama b";
}

if (! ($a == $b)) {
    echo "a tidak sama b";
}