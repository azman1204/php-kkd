<?php
# tertiary operator
$num = 10;
echo $num > 15 ? "Greater than" : "Less than";

if ($num > 15) {
    echo "Greater than"; 
} else {
    echo "Less than";
}

// checking null
// ?? if null.. jika sebelah kiri null, execute sebelah kanan
// valid dlm php 8 shj
$name;
echo $name ?? 'tiada nama';