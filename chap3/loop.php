<?php
/**
 * jenis2 loop dlm PHP
 * 1. foreach
 * 2. while
 * 3. for
 * 4. do..while
 */

 // for loop
 for ($i = 0; $i < 10; $i++) {
    echo "$i <br>";
 }

 $no = 1;
 while($no < 10) {
    echo "$no";
    $no++; // $no = $no + 1
 }

 echo "<hr>";
 // loop khas utk array
 $arr = [1,2,3,4,5,6,7,8,9];
 foreach($arr as $bil) {
    echo $bil;
 }

 
 for ($i = 0; $i < count($arr); $i++) {
    echo $arr[$i];
 }