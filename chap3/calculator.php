<?php
$investment_amount = 1000000;
$return_rate = 5.5/100; // 10%
$duration = 10; // years
$year = 2023;
// echo '<hr>';
for($i = 1; $i <= $duration; $i++) {
    echo  PHP_EOL;
    $profit = $return_rate * $investment_amount;
    
    // $investment_amount = $investment_amount + $profit;
    echo $year++ . " \t " . number_format($investment_amount,2) .  
        "\t " . number_format($profit, 2);
    $investment_amount += $profit;
}
// echo '<hr>';