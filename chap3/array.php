<?php
// NORMAL ARRAY
// set array
$names = []; // create empty array
$names[0] = 'Abu';
$names[1] = 'Ali';
$names[2] = 'Muthu';

//get from array
echo $names[1]; // Ali

$age = [30, 40, 50];
echo $age[1];

// array 2 dimensi (imagine ms excel)
$person = [
    ['Azman', 45],
    ['John Doe', 50]
];
// inject variable ke dlm string
// $person[row][col]
echo "<hr>";
echo "nama = {$person[0][0]} Umur = {$person[0][1]}";