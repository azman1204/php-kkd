<?php
// associative array. key => value

$person = [
    'name' => 'Azman Zakaria',
    'addr' => 'Bangi Lama'
];

$person['age'] = 45;
echo "Nama saya $person[name], umur {$person['age']} 
      dan saya 
      tinggal di {$person['addr']}";