<?php include '../master/header.php'; ?>

<form method="post" action="person_handler.php">
    <div class="row mb-2">
        <div class="col-2">Name:</div>
        <div class="col-6">
            <input type="text" name="name" required maxlength="10" class="form-control">
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-2">Salary:</div>
        <div class="col-6">
            <input type="number" name="salary" min='1500' max='10000' required class="form-control">
        </div>
    </div>
    
    <div class="row mb-2">
        <div class="col-2">Birth Date:</div>
        <div class="col-6">
            <input type="date" name="bod" 
            required class="form-control">
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-2">Username</div>
        <div class="col-6">
            <input type="text" name="username" 
            required class="form-control">
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-2">Password</div>
        <div class="col-6">
            <input type="password" name="password" 
            required class="form-control">
        </div>
    </div>

    <div class="row">
        <div class="col-2"></div>
        <div class="col-10"><input type="submit" 
        value="Simpan" class="btn btn-primary"></div>
    </div>
</form>

<?php include '../master/footer.php'; ?>