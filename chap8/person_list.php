<?php
include '../master/header.php';
// http://localhost/php-kkd/chap8/person_list.php
include 'connection.php';
$sql = "SELECT * FROM person";
// execute sql command
$result = mysqli_query($mysqli, $sql);
// mysqli_fetch_assoc() akan return data sehingga habis, then return false
// while ($rows = mysqli_fetch_assoc($result)) {
    //var_dump($rows);
    // echo $rows['name'];
// }
?>
<?php if ($_SESSION['role'] === 'adm') : ?>
    <a href="person_form.php" class="btn btn-info btn-sm mb-1">New Person</a>
<?php endif; ?>

<table class="table table-bordered table-striped table-hover table-secondary border-primary">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Salary</th>
        <th>BOD</th>
        <th>Tindakan</th>
    </tr>
    <?php 
    $no = 1;
    while ($rows = mysqli_fetch_assoc($result)) { ?>
        <tr>
            <td><?= $no++ ?></td>
            <td><?= $rows['name'] ?></td>
            <td><?= $rows['salary'] ?></td>
            <td><?= $rows['bod'] ?></td>
            <td>
                <a class="btn btn-danger btn-sm" href="person_delete.php?id=<?= $rows['id'] ?>">
                Delete
                </a>

                <a class="btn btn-outline-success btn-sm" href="person_edit.php?id=<?= $rows['id'] ?>">
                Edit
                </a>
            </td>
        </tr>
    <?php } ?>
</table>

<?php include '../master/footer.php' ?>

