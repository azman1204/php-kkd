<?php
include '../master/header.php';
include 'purchase.php';
?>
<form method="post" action="purchase_handler.php">
    <p class="display-4">Purchase Form</p>
    <div class="row mb-2">
        <div class="col-2">Item</div>
        <div class="col-4">
            <select name="item_id" class="form-control">
                <option>-- Sila Pilih --</option>
                <?php
                $items = Purchase::get_item();
                foreach ($items as $item_id => $name) {
                    echo "<option value='$item_id'>$name</option>";
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-2">Price</div>
        <div class="col-4">
            <input type="number" name="price" class="form-control">
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-2">Agree ?</div>
        <div class="col-4">
            <input type="radio" name="agree" value="Y" checked> Ya
            <input type="radio" name="agree" value="N"> Tidak
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-2">Extra Items</div>
        <div class="col-4">
            <input type="checkbox" name="extra_item[]" value="1"> Item 1
            <input type="checkbox" name="extra_item[]" value="2"> Item 2
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-2"></div>
        <div class="col-4">
            <input type="submit" class="btn btn-primary">
        </div>
    </div>
</form>
<?php
include '../master/footer.php';
?>