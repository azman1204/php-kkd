<?php 
include '../master/header.php';
include 'purchase.php';
?>
<div class="display-2 muted">Purchase List</div>

<a href="../purchasing/purchase_form.php" class="btn btn-success btn-sm">
    New Purchase
</a>

<table class="table">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Item Name</th>
            <th>Price</th>
            <th>Agreement</th>
            <th>Extra Item</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $purchase = new Purchase();
        $result = $purchase->list();
        while($row = mysqli_fetch_object($result)) {
        ?>
        <tr>
            <td></td>
            <td><?= $row->item_id ?></td>
            <td><?= $row->price ?></td>
            <td><?= $row->agree ?></td>
            <td><?= $row->extra_item ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php include '../master/footer.php' ?>