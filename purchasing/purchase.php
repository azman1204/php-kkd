<?php
/**
 * class ini handle semua data berkaitan purchase
 */
include '../chap8/connection.php';
class Purchase {
    function list() {
        global $mysqli;
        $sql = "SELECT * FROM purchase";
        $result = mysqli_query($mysqli, $sql);
        return $result;
    }

    // return array of items [1 => 'Basikal', ...]
    static function get_item() {
        global $mysqli;
        $arr = [];
        $sql = "SELECT * FROM item";
        $result = mysqli_query($mysqli, $sql);
        while ($row = mysqli_fetch_object($result)) {
            $arr[$row->id] = $row->name;
        }
        return $arr;
    }

    function insert($data) {
        global $mysqli;
        // handle checkbox
        if (isset($data['extra_item']))
            $extra = implode(',', $data['extra_item']);
        else
            $extra = '';
        $sql = "INSERT INTO 
        purchase(price, item_id, agree, extra_item, person_id) VALUES
        ($data[price], $data[item_id], '$data[agree]', '$extra', $data[person_id])";
        $result = mysqli_query($mysqli, $sql);
        return $result;
    }

}