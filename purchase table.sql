USE kkd_db;

CREATE TABLE purchase (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  item_id VARCHAR(50),
  price FLOAT,
  person_id int
);

INSERT INTO purchase(item_id, price, person_id) VALUES(1, 100, 1);
INSERT INTO purchase(item_id, price, person_id) VALUES(2, 50.5, 1);

SELECT * FROM purchase;
-- join 2 tables
SELECT * FROM person p1, purchase p2
WHERE p1.id = p2.person_id;

SELECT NAME, price FROM person p1, purchase p2
WHERE p1.id = p2.person_id;

