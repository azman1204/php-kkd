-- comment
-- SQL = sequal query language
-- CRUD

CREATE TABLE person (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name VARCHAR(50),
  bod date,
  salary float
);

-- reset table
TRUNCATE person;

-- Create
INSERT INTO person(NAME, salary, bod) VALUES('John Doe', 5000.20, '1970-01-01');
INSERT INTO person(NAME, salary, bod) VALUES('Abu Bakar Ellah', 4000, '1979-01-01');

-- Retrieve
SELECT * FROM person;

-- Update
UPDATE person SET NAME = 'Abu Bakar' WHERE id = 1;

-- Delete
DELETE from person WHERE id = 1;








